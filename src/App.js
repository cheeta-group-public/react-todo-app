import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import AddTodo from './components/Add_Todo/';
import Filter from './components/Filter/';

class App extends React.Component {
  render() {
    return (
      <div>
      <div> <AddTodo /> </div>
      <div> <Filter /> </div>
      </div>
    );
  }
}
export default App;
